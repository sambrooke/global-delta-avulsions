import rasterio
import numpy as np
import contextily as ctx
import geopandas as gpd
import rasterio
import rasterio.plot as rpl
from shapely.geometry import box, shape, mapping
from matplotlib import cm
import os
from PIL import Image
import datetime
import glob
from subprocess import call
from datetime import datetime
import ee
from skimage import exposure

import glob

# Frames directory
frame_root_dir = '/Volumes/Time Machine SB/NDWI_Jan_21_avulsions/NIR/NIR_annual'

output_dir = '/Volumes/Time Machine SB/NDWI_Jan_21_avulsions/NIR/NIR_frames_png'

dirs = os.listdir(frame_root_dir)[1:]

for d in dirs:
    tifs = glob.glob(os.path.join(frame_root_dir,d,'*.tif'))

    dates = []
    filename = []

    os.makedirs(os.path.join(output_dir, d), exist_ok=True)

    for t in tifs:

        print(t)

        with rasterio.open(t) as src:

            sensor = os.path.basename(t).split('_')[2]
            profile = src.profile.copy()

            width = profile['width']
            height = profile['height']

            epsg = str(src.crs).split(':')[1]

            profile.update({'nodata':0, 'count':1})

            dates = []
            dates_formated = []

            for dsc in src.descriptions:
                dt = datetime.strptime(dsc.split('_')[2], '%Y%m%d')
                dates.append(dt)
                dates_formated.append(dt.strftime('%b_%d_%Y'))

            for c in range(1, src.count):

                print(c)

                dat = src.read(c)
                # Contrast stretching
                p2 = np.percentile(dat, 2)
                p98 = np.percentile(dat, 98)
                img_rescale = exposure.rescale_intensity(dat, in_range=(p2, p98))

                im = Image.fromarray(np.uint8(cm.viridis(img_rescale)*255))
                im.save(os.path.join(output_dir, d, sensor+'_'+dates_formated[c-1]+'.png'))
