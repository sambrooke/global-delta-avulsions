source ~/.profile


STREAM=./river_profiles/parent_channel_courses/UTM/BS0115_1995.shp
DSM=./elevation_data/global_aoi_ALOS3D_30M/UTM_cropped_nofilter/BS0115_AW3D30.tif
WIDTH=420
INTERVAL=10
OUTPUT=./profiles_no_filter/BS0115_1995_profile_AW3D30.csv
OUTPUT_LINES=./transects/BS0115_1995_transects_AW3D30.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/BS0115_1995_interpolated_AW3D30.geojson
NODATA=-32768
ONLY_INTERP=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS --only-interp $ONLY_INTERP

                    

STREAM=./river_profiles/parent_channel_courses/UTM/CASP1_1991.shp
DSM=./elevation_data/global_aoi_ALOS3D_30M/UTM_cropped_nofilter/CASP1_AW3D30.tif
WIDTH=420
INTERVAL=10
OUTPUT=./profiles_no_filter/CASP1_1991_profile_AW3D30.csv
OUTPUT_LINES=./transects/CASP1_1991_transects_AW3D30.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/CASP1_1991_interpolated_AW3D30.geojson
NODATA=-32768
ONLY_INTERP=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS --only-interp $ONLY_INTERP

                    

STREAM=./river_profiles/parent_channel_courses/UTM/FIHERENANA.shp
DSM=./elevation_data/global_aoi_ALOS3D_30M/UTM_cropped_nofilter/FIHERENANA_AW3D30.tif
WIDTH=420
INTERVAL=10
OUTPUT=./profiles_no_filter/FIHERENANA_2001_profile_AW3D30.csv
OUTPUT_LINES=./transects/FIHERENANA_2001_transects_AW3D30.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/FIHERENANA_2001_interpolated_AW3D30.geojson
NODATA=-32768
ONLY_INTERP=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS --only-interp $ONLY_INTERP

                    

STREAM=./river_profiles/parent_channel_courses/UTM/TURK_002_1991.shp
DSM=./elevation_data/global_aoi_ALOS3D_30M/UTM_cropped_nofilter/TURK_002_AW3D30.tif
WIDTH=420
INTERVAL=10
OUTPUT=./profiles_no_filter/TURK_002_1991_profile_AW3D30.csv
OUTPUT_LINES=./transects/TURK_002_1991_transects_AW3D30.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/TURK_002_1991_interpolated_AW3D30.geojson
NODATA=-32768
ONLY_INTERP=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS --only-interp $ONLY_INTERP

                    