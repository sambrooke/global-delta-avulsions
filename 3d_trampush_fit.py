import numpy, scipy, scipy.optimize
from mpl_toolkits.mplot3d import  Axes3D
from matplotlib import cm # to colormap 3D surfaces from blue to red
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
import numpy as np
mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42


font = {'family' : 'arial',
        'weight' : 'normal',
        'size'   : 14}

mpl.rc('font', **font)
mpl.rcParams['pdf.fonttype'] = 42 # Truetype so text can be edited in Adobe Illustrator


trampush_2014 = pd.read_csv('3rd_party/trampush_2014_utf8.csv')
scour_table_global = pd.read_csv('3rd_party/S2_l_scour_table.csv')

Abf = trampush_2014['Abf [m2]']
Qbf = trampush_2014['Qbf [m3/s]']
Wbf = trampush_2014['Wbf [m]']
Hbf = trampush_2014['Hbf [m]']
slope = trampush_2014['S [-]']
D50_mm = trampush_2014['D50 [mm]']
D50 = trampush_2014['D50 [mm]']/1000

Hbf_deltas = scour_table_global['Hbf [m]']
slope_deltas = scour_table_global['S [-]']
D50_deltas = scour_table_global['D50 [m]']

D50.append(D50_deltas)
Hbf.append(Hbf_deltas)
slope.append(slope_deltas)

D50_lt_1mm_mask = D50 <= 0.001
D50=D50[D50_lt_1mm_mask]
Hbf=Hbf[D50_lt_1mm_mask]
slope=slope[D50_lt_1mm_mask]

graphWidth = 800 # units are pixels
graphHeight = 600 # units are pixels

# 3D contour plot lines
numberOfContourLines = 16

xData = Hbf
yData = slope
zData = D50

# place the data in a single list
data = [xData, yData, zData]


def SurfacePlot(func, data, fittedParameters):
    f = plt.figure(figsize=(graphWidth/100.0, graphHeight/100.0), dpi=100)

    mpl.pyplot.grid(True)
    axes = Axes3D(f)

    # extract data from the single list
    x_data = np.log10(data[0])
    y_data = np.log10(data[1])
    z_data = np.log10(data[2])

    xModel = numpy.linspace(min(x_data), max(x_data), 20)
    yModel = numpy.linspace(min(y_data), max(y_data), 20)
    X, Y = numpy.meshgrid(xModel, yModel)

    Z = func(numpy.array([X, Y]), *fittedParameters)

    axes.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=1, antialiased=True)

    axes.scatter(x_data, y_data, z_data) # show data along with plotted surface

    axes.set_title('Surface Plot (click-drag with mouse)') # add a title for surface plot
    axes.set_xlabel('Hbf')
    axes.set_ylabel('Slope')
    axes.set_zlabel('D50')
    plt.show()
    plt.close('all') # clean up after using pyplot or else there can be memory and process problems


def ContourPlot(func, data, fittedParameters):
    f = plt.figure(figsize=(graphWidth/100.0, graphHeight/100.0), dpi=100)
    axes = f.add_subplot(111)

    # extract data from the single list
    x_data = np.log10(data[0])
    y_data = np.log10(data[1])
    z_data = np.log10(data[2])

    xModel = numpy.linspace(min(x_data), max(x_data), 20)
    yModel = numpy.linspace(min(y_data), max(y_data), 20)
    X, Y = numpy.meshgrid(xModel, yModel)

    Z = func(numpy.array([X, Y]), *fittedParameters)

    axes.plot(x_data, y_data, 'o')

    axes.set_title('Contour Plot') # add a title for contour plot
    axes.set_xlabel('X Data') # X axis data label
    axes.set_ylabel('Y Data') # Y axis data label

    CS = mpl.pyplot.contour(X, Y, Z, numberOfContourLines, colors='k')
    mpl.pyplot.clabel(CS, inline=1, fontsize=10) # labels for contours

    plt.show()
    plt.close('all') # clean up after using pyplot or else there can be memory and process problems


def ScatterPlot(data):
    f = plt.figure(figsize=(graphWidth/100.0, graphHeight/100.0), dpi=100)

    mpl.pyplot.grid(True)
    axes = Axes3D(f)

    # extract data from the single list
    x_data = np.log10(data[0])
    y_data = np.log10(data[1])
    z_data = np.log10(data[2])

    axes.scatter(x_data, y_data, z_data)

    axes.set_title('Scatter Plot (click-drag with mouse)')
    axes.set_xlabel('Hbf')
    axes.set_ylabel('Slope')
    axes.set_zlabel('D50')

    plt.close('all') # clean up after using pyplot or else there can be memory and process problems


def func(X, a, b, c):
    x,y = X
    return a+b*x+c*y

    return


if __name__ == "__main__":
    initialParameters = [1.0, 1.0, 1.0] # these are the same as scipy default values in this example

    # here a non-linear surface fit is made with scipy's curve_fit()
    fittedParameters, pcov = scipy.optimize.curve_fit(func, [xData, yData], zData, p0 = initialParameters,  bounds=(-np.inf, np.inf))
    print(fittedParameters)
    ScatterPlot(data)
    SurfacePlot(func, data, fittedParameters)
#     ContourPlot(func, data, fittedParameters)

#     print('fitted parameters', fittedParameters)

#     modelPredictions = func(data, *fittedParameters)

#     absError = modelPredictions - zData

#     SE = numpy.square(absError) # squared errors
#     MSE = numpy.mean(SE) # mean squared errors
#     RMSE = numpy.sqrt(MSE) # Root Mean Squared Error, RMSE
#     Rsquared = 1.0 - (numpy.var(absError) / numpy.var(zData))
#     print('RMSE:', RMSE)
#     print('R-squared:', Rsquared)
