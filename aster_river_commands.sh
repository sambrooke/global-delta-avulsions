

STREAM=./river_profiles/parent_channel_courses/UTM/AFR0211_1998.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/AFR0211_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/AFR0211_1998_profile.csv
OUTPUT_LINES=./transects/AFR0211_1998_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/AFR0211_1998_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/ANGO2_2003.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/ANGO2_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/ANGO2_2003_profile.csv
OUTPUT_LINES=./transects/ANGO2_2003_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/ANGO2_2003_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/BS0115_1995.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/BS0115_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/BS0115_1995_profile.csv
OUTPUT_LINES=./transects/BS0115_1995_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/BS0115_1995_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/CA0067_1986.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/CA0067_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/CA0067_1986_profile.csv
OUTPUT_LINES=./transects/CA0067_1986_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/CA0067_1986_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA2_1984.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA2_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA2_1984_profile.csv
OUTPUT_LINES=./transects/JAVA2_1984_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA2_1984_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA3_1992.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA3_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA3_1992_profile.csv
OUTPUT_LINES=./transects/JAVA3_1992_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA3_1992_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA4_1973.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA4_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA4_1973_profile.csv
OUTPUT_LINES=./transects/JAVA4_1973_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA4_1973_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA5_1995.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA5_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA5_1995_profile.csv
OUTPUT_LINES=./transects/JAVA5_1995_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA5_1995_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA5_1998.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA5_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA5_1998_profile.csv
OUTPUT_LINES=./transects/JAVA5_1998_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA5_1998_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA5_2018.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA5_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA5_2018_profile.csv
OUTPUT_LINES=./transects/JAVA5_2018_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA5_2018_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA6_2010.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA6_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA6_2010_profile.csv
OUTPUT_LINES=./transects/JAVA6_2010_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA6_2010_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA7_1982.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA7_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA7_1982_profile.csv
OUTPUT_LINES=./transects/JAVA7_1982_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA7_1982_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA7_1990.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA7_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA7_1990_profile.csv
OUTPUT_LINES=./transects/JAVA7_1990_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA7_1990_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA7_2010.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA7_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA7_2010_profile.csv
OUTPUT_LINES=./transects/JAVA7_2010_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA7_2010_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA8_2018.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA8_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA8_2018_profile.csv
OUTPUT_LINES=./transects/JAVA8_2018_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA8_2018_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA9_2014.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA9_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA9_2014_profile.csv
OUTPUT_LINES=./transects/JAVA9_2014_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA9_2014_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/JAVA12_1985.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/JAVA12_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/JAVA12_1985_profile.csv
OUTPUT_LINES=./transects/JAVA12_1985_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/JAVA12_1985_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/MALAWI1_2014.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/MALAWI1_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/MALAWI1_2014_profile.csv
OUTPUT_LINES=./transects/MALAWI1_2014_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/MALAWI1_2014_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/PI0019_1989.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/PI0019_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/PI0019_1989_profile.csv
OUTPUT_LINES=./transects/PI0019_1989_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/PI0019_1989_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/PI0209_2006.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/PI0209_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/PI0209_2006_profile.csv
OUTPUT_LINES=./transects/PI0209_2006_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/PI0209_2006_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/PI0769_1982.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/PI0769_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/PI0769_1982_profile.csv
OUTPUT_LINES=./transects/PI0769_1982_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/PI0769_1982_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/SA0210_1996.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/SA0210_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/SA0210_1996_profile.csv
OUTPUT_LINES=./transects/SA0210_1996_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/SA0210_1996_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/SA0505_1999.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/SA0505_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/SA0505_1999_profile.csv
OUTPUT_LINES=./transects/SA0505_1999_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/SA0505_1999_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/SUMA2_2005.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/SUMA2_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/SUMA2_2005_profile.csv
OUTPUT_LINES=./transects/SUMA2_2005_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/SUMA2_2005_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/VENEZ1_1982.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/VENEZ1_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/VENEZ1_1982_profile.csv
OUTPUT_LINES=./transects/VENEZ1_1982_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/VENEZ1_1982_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/VENEZ2_1986.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/VENEZ2_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/VENEZ2_1986_profile.csv
OUTPUT_LINES=./transects/VENEZ2_1986_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/VENEZ2_1986_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/VENEZ2_2009.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/VENEZ2_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/VENEZ2_2009_profile.csv
OUTPUT_LINES=./transects/VENEZ2_2009_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/VENEZ2_2009_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/CA0194_2007.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/CA0194_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/CA0194_2007_profile.csv
OUTPUT_LINES=./transects/CA0194_2007_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/CA0194_2007_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        

STREAM=./river_profiles/parent_channel_courses/UTM/CASP1_1991.shp
DSM=./global_aoi_ASTER_GDM/UTM_cropped/CASP1_ASTGTMV003.tif
WIDTH=500
INTERVAL=10
OUTPUT=./profiles/CASP1_1991_profile.csv
OUTPUT_LINES=./transects/CASP1_1991_transects.geojson
OUTPUT_PATHS=./river_profiles/parent_channel_courses/UTM_interpolated/CASP1_1991_interpolated.geojson
NODATA=0
python3 IQR_profiler.py $STREAM $DSM $WIDTH $OUTPUT --interval $INTERVAL --output-lines $OUTPUT_LINES --nodata $NODATA --output-interpolated-path $OUTPUT_PATHS

        